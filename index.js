// Se define un método local, para leer contenido Web
var url = "https://si3.bcentral.cl/bdemovil/BDE/Series/MOV_SC_PR11";

leerWebPage: function (url, callback) {
  var request = require('request');
  var cheerio = require('cheerio');

  request(url, function (err, resp, html) {
    if (!err) {
      // Se define instancia a Cheerio
      const $ = cheerio.load(html);
      // Obtener contenedores de las fechas y valores de la página
      let container = $('.scrollTable tr')
      
      // Obtener fechas de los contenedores
      let fecha = []
      container.map( function(index, fecha) {
        let fecha = $($(container).find('.text-center')[0]).text()
        // console.log(texto)
        fecha.push(fecha)
      })
      let valor = []
      container.map( function(index, fecha) {
        let valor = $($(container).find('.text-right')[0]).text()
        // console.log(texto)
        valor.push(valor)
      })

      // Se retorna fecha
      callback(null, fecha)

      // Se retorna valor
      callback(null, valor)

    } else {
      console.log("Se produjo un error al leer la URL: " + url, err);
      callback(err)
    }
  })
  
};





// -- csv -- //

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
    path: 'file.csv',
    header: [
        {id: 'fecha', title: 'FECHA'},
        {id: 'valor', title: 'VALOR'}   
    ]
});

var fecha
var valor

const records = [
    {fecha: fecha, valor: valor}
];
 
csvWriter.writeRecords(records)
    .then(() => {
        console.log('...csv file created! :)');
    });










  
  contarMenciones: function (textos) {
    // Se muestra el contenido de la variable local
    // console.log("Jugadores considerados", this.jugadores)

    // Recorrer los valores del arreglo local
    let cantidad = this.jugadores.map( (jugador)=>{
      textos.forEach(elemento => {
        if (elemento.indexOf(jugador.name) > -1) jugador.mentions++
      })

      return jugador
    })

    // Retornar / Devolver el dato procesado
    return cantidad
  },
  
  // Se define un método local, para escribir CSV
  escribirCSV: function (data){
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    const csvWriter = createCsvWriter({
      path: 'jugadores.csv',
      header: [
        { id: 'name', title: 'Jugador' },
        { id: 'mentions', title: 'Menciones' },
      ]
    })

    csvWriter
      .writeRecords(data)
      .then(() => console.log('Archivo CSV de jugadores guardado.'))
      .catch((err) => console.log('Error al crear archivo CSV de jugadores.', err))

  },

  // Se define un método local, para escribir CSV
  escribirJSON: function (data){
    const fs = require('fs');

    fs.writeFile('mensiones.json', data, 'utf8', function(){
      console.log('Archivo json de jugadores.')
    })
  }
}