# Examen FAU

Extracción de valor de UF diarios desde 01-01-2013 a la fecha (Banco Central)

___

### Misión

1. Fuente de datos
2. Procesar algún dato de esa fuente
3. Exportar datos a un csv u otro formato
4. Mostrarlo en un gráfico o mapa
5. Gitlab (o github)
6. Netlify (o gitbub pages)

___

### Resumen de mi trabajo

Desde la página del Banco Central, quiero extraer el día, y el valor (pesos) de la UF y con esto, hacer un gráfico en mi landing page.

**FUENTE:** https://si3.bcentral.cl/bdemovil/BDE/Series/MOV_SC_PR11

___

### Pasos a seguir
1. Creación de repositorio (gitlab) con readme (https://gitlab.com/triniswinburn/examen_fau)
2. Clonar repositorio en escritorio
3. Creación de index.html, index.js, carpeta css, carpeta js, carpeta images
4. Creación de package.json
5. Publicar en netlify

___

### Instalar recursos
1. node
2. npm
3. cheerio
4. request
5. chart
6. cvs-writer

___

### Selectores

##### Día

`#datosSeries .scrollTable tr td.text-center`

`$('#datosSeries .scrollTable tr td.text-center').text();`

##### Valor en peso

`#datosSeries .scrollTable tr td.text-right`

`$('#datosSeries .scrollTable tr td.text-right').text();`

___

### Para extraer datos para el cvs

Hay que hacer un .map que recorra la info de la fuente y le haga push al .csv

**FUENTE:** https://si3.bcentral.cl/bdemovil/BDE/Series/MOV_SC_PR11

___

### Para generar archivo .csv

`node index.js`

___


